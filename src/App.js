import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { CustomButton, CustomTextInput } from './components';


const App = () => {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <CustomTextInput label={'We are the best, agree?'} />
        <CustomButton label={'TOBIKO'} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
   marginHorizontal:30
  }
});

export default App;
