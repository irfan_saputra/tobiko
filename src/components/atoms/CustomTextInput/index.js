import React from 'react';
import { View, TextInput, StyleSheet, Text } from 'react-native';

const CustomTextInput = ({label,placeholder}) => {
    return (
        <View>
            <Text style={style.label}>{label}</Text>
            <TextInput style={style.input} placeholder={placeholder}/>
        </View>
    )
}
const style = StyleSheet.create({
    label:{
        fontSize:14,
        marginBottom:10
    },
    input: {
        backgroundColor: '#F3F1F1',
        borderRadius:8,
        padding:10
    },

})
export default CustomTextInput;