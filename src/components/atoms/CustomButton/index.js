import React from 'react';
import { View, TextInput, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';

//import logosignin from '../../../assets/secondpage.png'

const CustomButton = ({ label, color = '#FFA451', textcolor = 'white', onPress }) => {
    return (
        <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
            <View style={style.container(color)}>
                <Text style={style.label(textcolor)}>{label}</Text>
            </View>
        </TouchableOpacity>
    )
}
const style = StyleSheet.create({
    container: (color) => ({
        backgroundColor: color,
        //backgroundColor: '#FFA451',
        padding: 10,
        borderRadius: 8
    }),
    label: (textcolor) => ({
        color: textcolor,
        textAlign: 'center'
    })

})
export default CustomButton;